@extends('layouts.app')

@section('title')
	Dashboard
	<small>Menu Utama</small>
@endsection

@section('breadcrumb')
	<li class="active">Dashboard</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body">
					<h1>Selamat Datang,</h1>
					<h2>Anda login sebagai <b>Admin</b></h2>
				</div>
			</div>
		</div>
	</div>
@endsection