@extends('layouts.app')

@section('title')
	Daftar Kategori
@endsection

@section('breadcrumb')
	<li class="active">Kategori</li>
@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header">
					<a href="#" onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
				</div>
				<div class="box-body">
					<table class="table table-striped">
						<thead>
							<tr>
								<th width="30">No</th>
								<th>Nama Kategori</th>
								<th width="100">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@include('kategori.form_kategori')
	<meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('script')
	<script type="text/javascript">
		var table, save_method;
		$(function(){
			table = $('.table').DataTable({
				"processing": true,
				"ajax": {
					"url": "{{ route('kategori.data') }}",
					"type": "GET"
				}
			});

			$('#modal-form form').validator().on('submit', function(e){
				$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			        }
			    });
			    
				if(!e.isDefaultPrevented()){
					var id = $('#id').val();
					if(save_method == "add"){
						url = "{{ route('kategori.store') }}";
						type = "POST";
					}else{
						url = "kategori/"+id;
						type = "PUT";
					}

					$.ajax({
						url: url,
						type: type,
						data: $('#modal-form form').serialize(),
						success: function(data){
							table.ajax.reload();
							$('#modal-form').modal('hide');
						},
						error: function(){
							alert("Tidak dapat menyimpan data!");
						}
					});
					return false;
				}
			});
		});

		function addForm(){
			save_method = "add";
			$('input[name=method]').val('POST');
			$('#modal-form').modal('show');
			$('#frmKategori')[0].reset();
			$('.modal-title').text('Tambah Kategori');
		}

		function editForm(id){
			save_method = "edit";
			$('input[name=method]').val('PATCH');
			$('#frmKategori')[0].reset();
			$.ajax({
				url: "kategori/"+id+"/edit",
				type: "GET",
				dataType: "JSON",
				success: function(data){
					$('#modal-form').modal('show');
					$('.modal-title').text('Edit Kategori');
					$('#id').val(data.id_kategori);
					$('#nama').val(data.nama_kategori);
				},
				error: function(){
					alert("Tidak dapat menampilkan data!");
				}
			});
		}

		function deleteData(id){
			if(confirm("Apakah yakin data akan dihapus ?")){
				$.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			        }
			    });

				$.ajax({
					url: "kategori/"+id,
					type: "DELETE",
					data: {'_token' : $('meta[name=csrf-token]').attr('content')},
					success: function(data){
						table.ajax.reload();
					},
					error: function(){
						alert("Tidak dapat menghapus data!");
					}
				});
			}
		}
	</script>
@endsection