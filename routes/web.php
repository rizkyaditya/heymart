<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::group(['middleware' => ['web', 'cekLevel:1']], function(){
	// KATEGORI
	Route::get('kategori/data', 'coKategori@listData')->name('kategori.data');
	Route::resource('kategori', 'coKategori');
	Route::get('kategori', 'coKategori@index')->name('kategori');
	// PRODUK
	Route::get('produk/data', 'coProduk@listData')->name('produk.data');
	Route::post('produk/hapus', 'coProduk@deleteSelected')->name('produk.hapus');
	Route::post('produk/cetak', 'coProduk@printBarcode')->name('produk.cetak');
	Route::resource('produk', 'coProduk');
	Route::get('produk', 'coProduk@index')->name('produk');
});

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/produk', 'coKategori@index')->name('produk');
// Route::get('/member', 'coKategori@index')->name('member');
// Route::get('/supplier', 'coKategori@index')->name('supplier');
// Route::get('/pengeluaran', 'coKategori@index')->name('pengeluaran');
// Route::get('/user', 'coKategori@index')->name('user');
// Route::get('/penjualan', 'coKategori@index')->name('penjualan');
// Route::get('/pembelian', 'coKategori@index')->name('pembelian');
// Route::get('/laporan', 'coKategori@index')->name('laporan');
// Route::get('/setting', 'coKategori@index')->name('setting');
// Route::get('/transaksi', 'coKategori@index')->name('transaksi');
