<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class moProduk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'id_produk';

    public function produk(){
    	return $this->belongsTo('App\moKategori', 'id_kategori');
    }
}
